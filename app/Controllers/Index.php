<?php

namespace Controllers;

/**
 * Description of Index
 *
 * @author Robert.Bednarowski
 */
class Index
{

    protected $f3;
    protected $db;

    public function __construct()
    {
        $this->f3 = \Base::instance();
        $this->db = new \DB\SQL($this->f3->get('DB.dns'), $this->f3->get('DB.user'), $this->f3->get('DB.pass'));
    }

    public function index()
    {     
        $lead = new \DB\SQL\Mapper($this->db, 'lead');
        if (!empty($this->f3->get('POST'))) {
            $errors = $this->validate($this->f3->get('POST'));
            $lead->copyFrom('POST');
            if($errors['valid'] == 1) {                
                $lead->timestamp = date("Y-m-d H:i:s");
                $lead->save();
                $file = $this->createXML($lead);
                $this->sendXML($file);
            }     
        }
        
        $this->f3->set('content', 'home.phtml');
        $this->f3->set('lead', $lead);
        if(isset($errors)) {
            $this->f3->set('errors', $errors);
        }
        echo \View::instance()->render('layout.phtml');
    }

    private function createXML($lead)
    {
        $elements = array(
            'reatilerid',
            'lead_source',
            'lead_type',
            'campaignid',
            'campaignname',
            'mcpcode',
            'contactdate',
            'al_dms',
            'ucid',
            'smd',
            'salutation',
            'firstname',
            'lastname',
            'birthdate_or_year',
            'companyname',
            'function',
            'occupation',
            'docmail',
            'docphonefax',
            'docemail',
            'customerflag',
            'comment',
            'addresstype',
            'address',
            'address_ext',
            'zipcode',
            'city',
            'country',
            'phone',
            'phone2',
            'mobile',
            'fax',
            'emailaddress',
            'emailaddress2',
            'veh_brand1',
            'veh_type1',
            'veh_body1',
            'veh_firstregistrationdate1',
            'veh_milage1',
            'vi_brand',
            'vi_purchasetype',
            'vi_type',
            'vi_body',
            'vi_annotation',
            'vi_pa',
            'testdrive',
            'financing',
            'consultation',
            'trade_in',
            'contact_method'
        );

        $mbfsElements = array(
            'contract_number',
            'startdate',
            'enddate',
            'duration',
            'brand',
            'model',
            'new_or_used',
            'financed_amount',
            'initial_capital',
            'vin',
            'financial_product',
            'customer_number',
            'salesperson',
            'financial_rent',
            'aftersales_rent',
            'insurance_d_rent',
            'insurance_cf_rent',
            'insurance_gvn_rent',
            'buy_back_value',
            'offer_desc',
            'offer_flag'
        );
        /* Version 1 create xml */
        $dir = $this->f3->get('ROOT') . $this->f3->get('BASE') . '/files/';
        mkdir($dir . $lead->lead_id);
        $file = $dir . $lead->lead_id . '/file.xml';

        $xml = new \DOMDocument('1.0', 'UTF-8');

        $mbflead = $xml->createElement('mbflead');
        $mbfleadAttribute = $xml->createAttribute('xmlns');
        $mbfleadAttribute->value = "xsd.mbf.cmx.consol.com";
        $mbflead->appendChild($mbfleadAttribute);


        foreach ($elements as $element) {
            $xmlElement = $xml->createElement($element);
            if ($element == "firstname") {
                $value = $xml->createTextNode($lead->name);
                $xmlElement->appendChild($value);
            }
            if ($element == "lastname") {
                $value = $xml->createTextNode($lead->surname);
                $xmlElement->appendChild($value);
            }
            $mbflead->appendChild($xmlElement);
        }

        $mbfs = $xml->createElement('mbfs');
        foreach ($mbfsElements as $mbfsElement) {
            $xmlMbfsElement = $xml->createElement($mbfsElement);
            $mbfs->appendChild($xmlMbfsElement);
        }
        $mbflead->appendChild($mbfs);

        $xml->appendChild($mbflead);

        $xml->formatOutput = true;
        $xml->save($file);
        
        
        /* Version 2 load from file */
        $baseFile = $dir.'file.xml';
        $file2 = $dir . $lead->lead_id . '/file2.xml';
        
        $xml2 = new \DomDocument();
        $xml2->load($baseFile);
        $mbfleads = $xml2->getElementsByTagName('mbflead'); 
        $firstNames = $xml2->getElementsByTagName('firstname'); 
        $lastNames = $xml2->getElementsByTagName('lastname'); 
        foreach($mbfleads as $mbflead) {
            $mbfleadAttribute = $xml2->createAttribute('xmlns');
            $mbfleadAttribute->value = "xsd.mbf.cmx.consol.com";
            $mbflead->appendChild($mbfleadAttribute);
        }
     
        foreach($firstNames as $firstName) {
            $value = $xml2->createTextNode($lead->name);            
            $firstName->appendChild($value);
        }
        
        foreach($lastNames as $lastName) {
            $value = $xml2->createTextNode($lead->surname);            
            $lastName->appendChild($value);
        }
        
        
        $xml2->save($file2);
        
        
        return $file;
    }

    private function sendXml($file)
    {            
        require_once('/app/PHPMailer/class.phpmailer.php');
        include("/app/PHPMailer/class.smtp.php");
        $mail = new \PHPMailer(true); 
        $mail->isSMTP();
        $mail->Host = 'poczta.o2.pl';  
        $mail->SMTPAuth = true;                               
        $mail->Username = 'sir_ad@o2.pl';                 
        $mail->Password = '';                           
        $mail->SMTPSecure = 'ssl';                            
        $mail->Port = 465;      
        $mail->From = 'sir_ad@o2.pl';
        $mail->FromName = 'Robert';
        $mail->addAddress('robert.bednarowski@comeandstay.com');
        $mail->Subject = 'Here is the subject';
        $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
        $mail->AddAttachment($file, "file.xml"); 
     
        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
             
    }
    
    
    private function validate($data)            
    {      
        
        $errors = array();
        $errors['valid'] = 1;
        if(!isset($data['civ'])) {
            $data['civ'] = '';
        }
        foreach($data as $field => $value)
        {
            switch ($field) {
                case 'name' :
                    if($this->required($value)) {
                        $errors['name']['msg'] = 'Name is required';
                        $errors['name']['valid'] = 0;
                        $errors['valid'] = 0;
                    }
                    break;
                case 'surname' :
                    if($this->required($value)) {
                        $errors['surname']['msg'] = 'Surname is required';
                        $errors['surname']['valid'] = 0;
                        $errors['valid'] = 0;
                    }
                    break;    
                case 'civ' :
                    if($this->required($value)) {
                        $errors['civ']['msg'] = 'Civ is required';
                        $errors['civ']['valid'] = 0;
                        $errors['valid'] = 0;
                    }
                    break;        
                case 'phone' :
                    if($this->required($value)) {
                        $errors['phone']['msg'] = 'Phone is required';
                        $errors['phone']['valid'] = 0;
                        $errors['valid'] = 0;
                    }
                    break;    
                case 'zipcode' :
                    if($this->required($value)) {
                        $errors['zipcode']['msg'] = 'Zipcode is required';
                        $errors['zipcode']['valid'] = 0;
                        $errors['valid'] = 0;
                    }
                    break;                
            }
                
        }    
        return $errors;
    }
    
    private function required($data)
    {
        return (empty($data));
    }

}
